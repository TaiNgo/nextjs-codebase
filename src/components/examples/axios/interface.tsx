interface Data {
    id: string;
    setup: string;
    punchline: string;
}

export type { Data };
